package snmptrig;

import java.util.Map;

public class PostModel {

	int version;
	String groupKey;
	String status;
	String receiver;
	Map<String, Object> groupLabels;
	Map<String, Object> commonLabels;
	Map<String, Object> commonAnnotations;
	String externalURL;
	AlertModel[] alerts;

	public int getVersion() {
		return version;
	}
	public String getGroupKey() {
		return groupKey;
	}
	public String getStatus() {
		return status;
	}
	public String getReceiver() {
		return receiver;
	}
	public Map<String, Object> getGroupLabels() {
		return groupLabels;
	}
	public Map<String, Object> getCommonLabels() {
		return commonLabels;
	}
	public Map<String, Object> getCommonAnnotations() {
		return commonAnnotations;
	}
	public String getExternalURL() {
		return externalURL;
	}
	public AlertModel[] getAlerts() {
		return alerts;
	}

}
