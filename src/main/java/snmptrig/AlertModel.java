package snmptrig;

import java.util.Map;

public class AlertModel {

	String status;
	Map<String, Object> labels;
	Map<String, Object> annotations;
	String startsAt;
	String endsAt;
	String generatorURL;

	public String getStatus() {
		return status;
	}
	public Map<String, Object> getLabels() {
		return labels;
	}
	public Map<String, Object> getAnnotations() {
		return annotations;
	}
	public String getStartsAt() {
		return startsAt;
	}
	public String getEndsAt() {
		return endsAt;
	}
	public String getGeneratorURL() {
		return generatorURL;
	}

}
