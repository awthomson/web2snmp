package snmptrig;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;


@RestController
public class SNMPTrigController {

    String resp = "hello";	

    public SNMPTrigController() {

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "/trigger", method = RequestMethod.POST)
    public ResponseEntity trigger(@RequestBody PostModel inputModel) {
	PostModel response = new PostModel();

	System.out.println("          Version: "+inputModel.getVersion());
	System.out.println("         groupKey: "+inputModel.getGroupKey());
	System.out.println("           status: "+inputModel.getStatus());
	System.out.println("         receiver: "+inputModel.getReceiver());
	System.out.println("      groupLabels: "+inputModel.getGroupLabels());
	System.out.println("     commonLabels: "+inputModel.getCommonLabels());
	System.out.println("commonAnnotations: "+inputModel.getCommonAnnotations());
	System.out.println("           alerts: "+inputModel.getAlerts());

        return new ResponseEntity(response, HttpStatus.OK);
    }
}
